FROM fedora:latest
LABEL maintainer Chris Collins <christopher.collins@duke.edu>

LABEL name "hashicorp-vault-container"
LABEL version "1.0"
LABEL release "0"
LABEL Summary "Hashicorp Vault in a Container"
LABEL Description "Vault secures, stores, and tightly controls access to tokens, passwords, certificates, API keys, and other secrets in modern computing."
LABEL authoritative-source-url "https://gitlab.oit.duke.edu/clc0317/hashicorp-vault-container"
LABEL changelog-url "https://gitlab.oit.duke.edu/clc0317/hashicorp-vault-container/activity"

ARG VAULT_VER=0.9.5
ENV VAULT_URL=https://releases.hashicorp.com/vault/${VAULT_VER}
ENV VAULT_PKG=vault_${VAULT_VER}_linux_amd64.zip
ENV VAULT_SUM=vault_${VAULT_VER}_SHA256SUMS
ENV VAULT_SIG=vault_${VAULT_VER}_SHA256SUMS.sig

ENV INSTALLED_PACKAGES="gnupg perl-Digest-SHA zip python3-PyYAML yamllint"

ADD . /

RUN dnf install -y ${INSTALLED_PACKAGES} \
      && dnf clean all \
      && rm -rf /var/cache/yum

RUN gpg --import hashicorp.asc \
      && curl -Os ${VAULT_URL}/${VAULT_PKG} \
      && curl -Os ${VAULT_URL}/${VAULT_SUM} \
      && curl -Os ${VAULT_URL}/${VAULT_SIG}

RUN gpg --verify ${VAULT_SIG} ${VAULT_SUM} \
      && shasum -a 256 -c ${VAULT_SUM} --ignore-missing

RUN unzip ${VAULT_PKG} -d /usr/bin

ENTRYPOINT ["/usr/bin/vault"]
CMD ["--help"]
