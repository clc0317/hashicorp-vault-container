Hashicorp Vault Container
=========================

Vault in a container

## Build/Install

```
# Build with the default 0.9.5 Vault version
docker-compose build

# Override the default version
docker-compose build --build-arg VAULT_VER=some.other.ver

# Or set the VAULT_VER build arg in docker-compose.yml or .env
```

## Upgrade

Upgrade Vault by replacing the VAULT_VER build argument with the latest version, or pull this repo and rebuild (assuming it's up to date; pull requests welcome).

## Run

Running with Docker Compose would be tedious, and require you to be in the build directory.  However, the default Dockerfile will build this image, named `vault`, so Docker can be used directly to run Vault.  The default `Entrypoint` is `vault`, so you only need to append your vault arguments:

```
docker run -it vault --version
```

## Run helper script

The `vault` bash script in this repository is a wrapper around the docker run command above, and attempts to run the Vault container in a way that behaves like a local binary.  If you have the `install` command on your system, you can install the script like so:

```
# If you have a ~/.local/bin, and it's in your PATH
install --mode 0775 vault ~/.local/bin/
```

Otherwise, just copy the file to some location in your PATH

## Testing

Rudimentary validation exists in this image.  You can test the vault install by running:

```
docker-compose build test && docker-compose run --entrypoint=/run-tests.py test
```
